function addTokens(input, tokens) {
    if (typeof input !== "string") {
      throw new Error("Invalid input");
    }
    if (input.length < 6) {
      throw new Error("Input should have at least 6 characters");
    }
  

    
    if (input.includes("...") === false) {
      return input;
    } else {
      let i = 0;
      var output = input.replace(
        /\.\.\./g,() => "${" + Object.values(tokens[i++]) + "}"
      );
      return output;
    }
   
  }
  
 
 var first = "Subsemnatul ... fewfwefwe ...";
  var second = [{ tokenName: "subsemnatul" }, { first: "fsdfsdfsdfdf" }];
  console.log(addTokens(first, second));

  const app = {
    addTokens: addTokens
  };
  
  module.exports = app;